package Examen;

import java.util.Scanner;

public class examen {
    public static void main(String[] args){
        
        Scanner scanner = new Scanner(System.in);
        
        int x = 0;
        
        while(x != 4){
            
            System.out.println("Elige una opción:");
            System.out.println("1.-\tCambio de horas a segundos.");
            System.out.println("2.-\tCambio de segundos a horas.");
            // Nuevas funcionalidades
            System.out.println("3.-\tCambio de km/h a m/s");
            System.out.println("4.-\tSalir.");
            
            System.out.println();
            System.out.println("Elige una opción:");
            x = scanner.nextInt();
            
            switch(x){
                // Simulamos que hago la versión alpha
                case 1:
                    System.out.println("Introduce las horas para pasarlo a segundos:");
                    int hours = scanner.nextInt();
                    
                    while(hours <= 0){
                        System.out.println("El tiempo no puede tener valor negativo:");
                        hours = scanner.nextInt();
                    }
                    
                    System.out.println(hours+" hora(s) son "+(hours*3600)+" segundos.");
                break;
                
                // Simulamos que hago la versión alpha
                case 2:
                    System.out.println("Introduce los segundos para pasarlo a horas:");
                    int seconds = scanner.nextInt();
                    
                    while(seconds <= 0){
                        System.out.println("El tiempo no puede tener valor negativo:");
                        seconds = scanner.nextInt();
                    }
                    
                    System.out.println(seconds+" segundos son "+((int) seconds/3600)+" horas");
                break;
                
                // Modificamos las nuevas funcionalidades
                // Simulamos que hago la versión alpha
                case 3:
                    System.out.println("Introduce los km/h para pasarlo a m/s");
                    int kmh = scanner.nextInt();
                    
                    while(kmh <= 0){
                        System.out.println("La velocidad no puede tener un valor negativo: ");
                        kmh = scanner.nextInt();
                    }
                    
                    System.out.println(kmh+"Km/h son "+((double) kmh/3.6)+"m/s");
                break;
                
                case 4:
                    System.out.println("Saliendo del programa.");
                break;
            }
            
        }
        
    }
}
